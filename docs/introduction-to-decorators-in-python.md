# 파이썬 데코레이터 소개: 초보자를 위한 간소화된 기능

## 데코레이터란?
데코레이터는 다른 함수나 메서드의 동작을 수정하는 데 사용할 수 있는 Python의 특수 함수이다. 데코레이터는 보통 데코레이터 함수 이름 앞에 `@` 기호를 사용하여 표시한다. 데코레이터는 함수를 입력으로 받아 일반적으로 원래 함수의 동작을 확장하거나 변경하는 새 함수를 반환한다.

## 간단한 데코레이터 만들기
데코레이터의 간단한 예부터 시작하겠다. 인사말 메시지를 출력하는 `say_hello` 함수가 있다고 가정해 보자.

```python
def say_hello():
    print("Hello, World!")

say_hello()
```

Output:

```
Hello, World!
```

이제 `say_hello` 함수에 몇 가지 추가 기능을 더하는 `decorate_hello`라는 데코레이터를 만들어 보겠다.

```python
def decorate_hello(func):
    def wrapper():
        print("Before calling the function")
        func()
        print("After calling the function")
    return wrapper

@decorate_hello
def say_hello():
    print("Hello, World!")
say_hello()
```

Output:

```
Before calling the function
Hello, World!
After calling the function
```

이 예에서는 `decorate_hello` 데코레이터가 `say_hello` 함수를 래핑하여 원래 함수를 호출하기 전과 후에 코드를 실행할 수 있도록 한다.

## 인수가 있는 데코레이터 사용하기
데코레이터는 인수를 받을 수도 있다. 함수 실행을 지정된 횟수만큼 반복하는 `repeat`이라는 데코레이터를 만들어 보겠다.

```python
def repeat(times):
    def decorator(func):
        def wrapper(*args, **kwargs):
            for _ in range(times):
                func(*args, **kwargs)
        return wrapper
    return decorator

@repeat(times=3)
def say_hello():
    print("Hello, World!")
say_hello()
```

Output:

```
Hello, World!
Hello, World!
Hello, World!
```

이 예에서 `repeat` 데코레이터는 데코레이션 함수를 실행할 횟수를 지정하는 인자 `times`를 받는다.

## 여러 데코레이터 체인 연결하기
여러 데코레이터를 하나의 함수에 연결하여 특정 순서로 적용할 수도 있다. 함수의 출력을 수정하기 위해 `uppercase`와 `exclamation`라는 두 가지 데코레이터를 만들어 보겠다.

```python
def uppercase(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return result.upper()
    return wrapper

def exclamation(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return f"{result}!"
    return wrapper

@exclamation
@uppercase
def say_hello():
    return "hello, world"
print(say_hello())
```

Output:

```
HELLO, WORLD!
```

이 예에서는 `exclamation` 데코레이터가 `uppercase` 데코레이터보다 먼저 적용되어 대문자 다음에 느낌표가 표시된다.

## 마치며
데코레이터는 함수나 메서드의 동작을 쉽게 향상하고 수정할 수 있는 Python의 강력한 도구이다. 데코레이터의 작동 원리, 생성 및 사용 방법을 이해하는 것은 모든 Python 개발자에게 필수적이다.
