# 파이썬 데코레이터 소개: 초보자를 위한 간소화된 기능 <sup>[1](#footnote_1)</sup>

Python의 데코레이터는 소스 코드를 변경하지 않고도 함수나 메서드의 동작을 수정하거나 향상시킬 수 있는 Python 프로그래밍의 강력하고 기본적인 개념이다. 데코레이터는 로깅, 인증, 성능 모니터링 등 다양한 용도로 Python에서 광범위하게 사용되고 있다. 이 포스팅에서는 초보자도 쉽게 이해할 수 있도록 Python에서 데코레이터를 세분화하여 설명한다. 각 단계마다 코드 예와 설명을 제공한다.

<a name="footnote_1">1</a>: [Introduction to Decorators in Python: Simplified for Beginners](https://python.plainenglish.io/introduction-to-decorators-in-python-simplified-for-beginners-b4c563acef27)를 편역한 것이다.
